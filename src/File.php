<?php

namespace App;

class File
{
    protected string $name;
    protected string $type;
    protected int $size;
    protected string $tmpName;
    protected string $error;

    public function __construct(array $file)
    {
        $this->name=$file['name'];
        $this->type=$file['type'];
        $this->size=$file['size'];
        $this->tmpName=$file['tmp_name'];
        $this->error=$file['error'];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getSize(): string
    {
        return $this->size;
    }

    /**
     * @return string
     */
    public function getTmpName(): string
    {
        return $this->tmpName;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    public function upload(): bool
    {
        $fileUploader=new FileUploader();
        return $fileUploader->upload($this);
    }

}
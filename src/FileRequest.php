<?php

namespace App;
use App\File;

class FileRequest
{
    protected array $files;

    public function __construct()
    {
        echo "<pre>";
//        print_r($_FILES);
        $this->files = $_FILES;
    }

    public function get(string $key): ?File
    {
        return new File($this->files[$key]) ?? null;
    }
}